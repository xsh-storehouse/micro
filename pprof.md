####1. 下载graphviz
https://graphviz.gitlab.io/_pages/Download/windows/graphviz-2.38.msi

####2. 使用pprof
```cassandraql
    go func() {
            err:=http.ListenAndServe(":6060", nil)
            if err!=nil{
                log.Error("",err.Error())
            }
    }()
```

####3.火焰图生成
3.1
go tool pprof -http=:1234 http://localhost:6060/debug/pprof/profile
~~go tool pprof -http=:1235 http://localhost:6060/debug/pprof/heap?debug=1&seconds=120~~
3.2
curl -s http://localhost:6060/debug/pprof/heap?debug=1 > base.heap
等待时间
curl -s http://localhost:6060/debug/pprof/heap?debug=1 > current.heap
比较2个文件
go tool pprof --http :1234 --base base.heap current.heap


go tool pprof -inuse_space -cum -svg http://localhost:6060/debug/pprof/heap?debug=1 > heap_inuse.svg