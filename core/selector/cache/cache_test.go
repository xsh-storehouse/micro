package cache

import (
	"testing"

	"gitee.com/xsh-storehouse/micro/core/registry/mock"
	"gitee.com/xsh-storehouse/micro/core/selector"
)

func TestCacheSelector(t *testing.T) {
	counts := map[string]int{}

	cache := NewSelector(selector.Registry(mock.NewRegistry()))

	next, err := cache.Select("foo")
	if err != nil {
		t.Errorf("Unexpected error calling cache select: %s", err)
	}

	for i := 0; i < 100; i++ {
		node, err := next()
		if err != nil {
			t.Errorf("Expected node err, got err: %s", err)
		}
		counts[node.Id]++
	}

	t.Logf("Cache Counts %s", counts)
}
