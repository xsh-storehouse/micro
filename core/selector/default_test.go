package selector

import (
	"testing"

	"gitee.com/xsh-storehouse/micro/core/registry/mock"
)

func TestDefaultSelector(t *testing.T) {
	counts := map[string]int{}

	rs := newDefaultSelector(Registry(mock.NewRegistry()))

	next, err := rs.Select("foo")
	if err != nil {
		t.Errorf("Unexpected error calling default select: %s", err)
	}

	for i := 0; i < 100; i++ {
		node, err := next()
		if err != nil {
			t.Errorf("Expected node err, got err: %s", err)
		}
		counts[node.Id]++
	}

	t.Logf("Default Counts %s", counts)
}
