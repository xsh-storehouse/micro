package web

import (
	"gitee.com/xsh-storehouse/micro/core/conf"
	"gitee.com/xsh-storehouse/micro/core/module"
	basemodule "gitee.com/xsh-storehouse/micro/core/module/base"
	_ "gitee.com/xsh-storehouse/micro/core/web/boot"
	"gitee.com/xsh-storehouse/micro/core/web/router"
	_ 	"gitee.com/xsh-storehouse/micro/core/web/router"
	"gitee.com/xsh-storehouse/micro/utils/log"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/os/glog"
	"time"
)

type GF struct {
	basemodule.Module
	opts Options
}

func (w *GF) OnInit(subclass module.RPCModule, app module.App, settings *conf.Mods, opts ...Option) {
	w.opts = NewOptions(opts...)
	w.Module.OnInit(subclass, app, settings, w.opts.Opts...) //这是必须的
	if len(w.opts.DefaultInterface) != 0 {
		for _, v := range w.opts.DefaultInterface {
			router.DefaultInterface = append(router.DefaultInterface, v)
		}
	}

	if w.opts.HttpPort!=0{
		router.SetHttp(w.opts.HttpPort)
	}else{
		glog.Panicf("http port error %d",w.opts.HttpPort)
	}

	if w.opts.HttpsPort!=0{
		router.SetHttps(w.opts.HttpsPort)
	}else{
		glog.Panicf("http port error %d",w.opts.HttpPort)
	}
}

func (w *GF) Run(closeSig chan bool) {
	log.Info("%s模块运行中...", w.GetType())
	go func() {
		time.Sleep(time.Second)
		g.Server().Run()
	}()
	<-closeSig
	log.Info("%s模块已停止...", w.GetType())
}

func (w *GF) OnDestroy() {
	w.Module.OnDestroy() //这是必须的
}

func (w *GF) Options() Options {
	return w.opts
}

func (w *GF) GetType() string {
	return w.opts.Name
}
