package web

import (
	"gitee.com/xsh-storehouse/micro/core/server"
)

type Option func(*Options)

//Options 配置项
type Options struct {
	Name             string
	DefaultInterface []string
	HttpPort int
	HttpsPort int
	Opts             []server.Option
}

func NewOptions(opts ...Option) Options {
	opt := Options{}
	for _, o := range opts {
		o(&opt)
	}
	return opt
}

func Name(s string) Option {
	return func(o *Options) {
		o.Name = s
	}
}

func DefaultInterface(s []string) Option {
	return func(o *Options) {
		o.DefaultInterface = s
	}
}

func HttpPort(s int) Option {
	return func(o *Options) {
		o.HttpPort = s
	}
}

func HttpsPort(s int) Option {
	return func(o *Options) {
		o.HttpsPort = s
	}
}