package api

import (
	"gitee.com/xsh-storehouse/micro/core/web/app/api/home"
	"github.com/gogf/gf/net/ghttp"
)

func LoadRouter(s *ghttp.Server) {
	home.LoadRouter(s)
}
