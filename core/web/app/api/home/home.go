package home

import (
	"gitee.com/xsh-storehouse/micro/utils/response"
	"github.com/gogf/gf/net/ghttp"
)

func LoadRouter(s *ghttp.Server) {
	s.Group("/api/home", func(group *ghttp.RouterGroup) {
		group.GET("/index", Index)
	})
}

func Index(r *ghttp.Request) {
	response.Success(r).SetData("hello friend").Send()
}