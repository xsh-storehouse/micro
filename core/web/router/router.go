package router

import (
	"fmt"
	"gitee.com/xsh-storehouse/micro/core/web/app/api"
	"github.com/gogf/gf/frame/g"
	"os"
)

func init()  {
	s := g.Server()
	s.Use(CORS)
	api.LoadRouter(s)
	curDir, _ := os.Getwd()
	path :=fmt.Sprintf("%s\\core\\web",curDir)
	s.EnableHTTPS(path+"/public/https/server.crt", path+"/public/https/server.key")
}

func SetHttp(port int)  {
	s := g.Server()
	s.SetPort(port)
}

func SetHttps(port int)  {
	s := g.Server()
	s.SetHTTPSPort(port)
}