package router

import (
	"fmt"
	"gitee.com/xsh-storehouse/micro/utils/response"
	"gitee.com/xsh-storehouse/micro/utils/token"
	"github.com/gogf/gf/net/ghttp"
	"net/http"
	"strings"
)

//默认接口不做权限判断
var DefaultInterface = []string{"/admin/home", "/admin/home/login", "/admin/home/logout", "/admin/common/getCode", "/admin/common/uploadFile", "/admin/common/download"}

// 鉴权中间件，只有登录成功之后才能通过
func Auth(r *ghttp.Request) {
	url := r.Request.URL
	if IsDefaultInterface(url.Path) {
		r.Middleware.Next()
		return
	}
	//检测token
	stoken := r.Header.Get("token")
	if stoken != "" {
		if err := token.ValidateToken(stoken); err != nil {
			response.Rlogin(r).Send()
		} else {
			tokenUser := token.GetTokenUser(stoken)
			ip := r.GetClientIp()
			mkey := fmt.Sprintf("%s:%s", tokenUser.Name, ip)
			token := token.GetToken(mkey)
			if token != stoken {
				//response.Rlogin(r)
				r.Middleware.Next()
				return
			} else {
				r.Middleware.Next()
				return
			}
		}
	} else {
		response.Rlogin(r).Send()
	}
}

//判断是否是默认接口
func IsDefaultInterface(path string) bool {
	for i := range DefaultInterface {
		if strings.EqualFold(DefaultInterface[i], path) {
			return true
		}
	}
	return false
}

// 允许接口跨域请求
func CORS(r *ghttp.Request) {
	corsOptions := r.Response.DefaultCORSOptions()
	corsOptions.AllowOrigin = "*"
	corsOptions.AllowHeaders = "Content-Type,token,sign"
	if !r.Response.CORSAllowedOrigin(corsOptions) {
		r.Response.WriteStatus(http.StatusForbidden)
		return
	}
	r.Response.CORS(corsOptions)
	r.Middleware.Next()
}
