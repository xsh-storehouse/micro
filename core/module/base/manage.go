package base

import (
	"fmt"
	"gitee.com/xsh-storehouse/micro/core/conf"
	"gitee.com/xsh-storehouse/micro/core/module"
	"gitee.com/xsh-storehouse/micro/utils/log"
)

// NewModuleManager 新建模块管理器
func NewModuleManager() (m *ModuleManager) {
	m = new(ModuleManager)
	return
}

// ModuleManager 模块管理器
type ModuleManager struct {
	app     module.App
	mods    []*DefaultModule
	runMods []*DefaultModule
}

// Register 注册模块
func (mer *ModuleManager) Register(mi module.Module) {
	md := new(DefaultModule)
	md.mi = mi
	md.closeSig = make(chan bool, 1)

	mer.mods = append(mer.mods, md)
}

// RegisterRunMod 注册需要运行的模块
func (mer *ModuleManager) RegisterRunMod(mi module.Module) {
	md := new(DefaultModule)
	md.mi = mi
	md.closeSig = make(chan bool, 1)

	mer.runMods = append(mer.runMods, md)
}

// Init 初始化
func (mer *ModuleManager) Init(app module.App, Process string) {
	log.Info("this service moduleGroup(Process) is [%s]", Process)
	mer.app = app
	mer.CheckModuleSettings() //配置文件规则检查
	for i := 0; i < len(mer.mods); i++ {
		for Type, modSettings := range app.GetSettings().Mods {
			if mer.mods[i].mi.GetType() == Type {
				//匹配
				for _, setting := range modSettings {
					//这里可能有BUG 公网IP和局域网IP处理方式可能不一样,先不管
					if Process == setting.Process {
						mer.runMods = append(mer.runMods, mer.mods[i]) //这里加入能够运行的组件
						mer.mods[i].settings = setting
					}
				}
				break //跳出内部循环
			}
		}
	}

	for i := 0; i < len(mer.runMods); i++ {
		m := mer.runMods[i]
		m.mi.OnInit(app, m.settings)

		if app.GetModuleInited() != nil {
			app.GetModuleInited()(app, m.mi)
		}

		m.wg.Add(1)
		go run(m)
	}
	//timer.SetTimer(3, mer.ReportStatistics, nil) //统计汇报定时任务
}

// CheckModuleSettings module配置文件规则检查
// ID全局必须唯一
// 每一个类型的Module列表中ProcessID不能重复
func (mer *ModuleManager) CheckModuleSettings() {
	gid := map[string]string{} //用来保存全局ID-ModuleType
	for Type, modSettings := range conf.Conf.Mods {
		pid := map[string]string{} //用来保存模块中的 Process-ID
		for _, setting := range modSettings {
			if Stype, ok := gid[setting.ID]; ok {
				//如果Id已经存在,说明有两个相同Id的模块,这种情况不能被允许,这里就直接抛异常 强制崩溃以免以后调试找不到问题
				panic(fmt.Sprintf("ID (%s) been used in modules of type [%s] and cannot be reused", setting.ID, Stype))
			} else {
				gid[setting.ID] = Type
			}

			if id, ok := pid[setting.Process]; ok {
				//如果Id已经存在,说明有两个相同Id的模块,这种情况不能被允许,这里就直接抛异常 强制崩溃以免以后调试找不到问题
				panic(fmt.Sprintf("In the list of modules of type [%s], Process (%s) has been used for ID module for (%s)", Type, setting.Process, id))
			} else {
				pid[setting.Process] = setting.ID
			}
		}
	}
}

// Destroy 停止模块
func (mer *ModuleManager) Destroy() {
	for i := len(mer.runMods) - 1; i >= 0; i-- {
		m := mer.runMods[i]
		m.closeSig <- true
		m.wg.Wait()
		destroy(m)
	}
}
