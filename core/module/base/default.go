package base

import (
	"gitee.com/xsh-storehouse/micro/core/conf"
	"gitee.com/xsh-storehouse/micro/core/module"
	"gitee.com/xsh-storehouse/micro/utils/log"
	"runtime"
	"sync"
)

// DefaultModule 模块结构
type DefaultModule struct {
	mi       module.Module
	settings *conf.Mods
	closeSig chan bool
	wg       sync.WaitGroup
}

func run(m *DefaultModule) {
	defer func() {
		if r := recover(); r != nil {
			if conf.LenStackBuf > 0 {
				buf := make([]byte, conf.LenStackBuf)
				l := runtime.Stack(buf, false)
				log.Error("%s: %s", r, buf[:l])
			} else {
				log.Error("%s", r)
			}
		}
	}()
	m.mi.Run(m.closeSig)
	m.wg.Done()
}

func destroy(m *DefaultModule) {
	defer func() {
		if r := recover(); r != nil {
			if conf.LenStackBuf > 0 {
				buf := make([]byte, conf.LenStackBuf)
				l := runtime.Stack(buf, false)
				log.Error("%s: %s", r, buf[:l])
			} else {
				log.Error("%s", r)
			}
		}
	}()
	m.mi.OnDestroy()
}
