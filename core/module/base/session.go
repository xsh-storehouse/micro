package base

import (
	"context"
	"gitee.com/xsh-storehouse/micro/core/module"
	"gitee.com/xsh-storehouse/micro/core/registry"
	"gitee.com/xsh-storehouse/micro/core/rpc"
	"gitee.com/xsh-storehouse/micro/core/rpc/base"
)

// NewServerSession 创建一个节点实例
func NewServerSession(app module.App, name string, node *registry.Node) (module.ServerSession, error) {
	session := &serverSession{
		name: name,
		node: node,
		app:  app,
	}
	rpc, err := defaultrpc.NewRPCClient(app, session)
	if err != nil {
		return nil, err
	}
	session.rpc = rpc
	return session, err
}

type serverSession struct {
	node *registry.Node
	name string
	rpc  mqrpc.RPCClient
	app  module.App
}

func (c *serverSession) GetID() string {
	return c.node.Id
}

// Deprecated: 因为命名规范问题函数将废弃,请用GetID代替
func (c *serverSession) GetId() string {
	return c.node.Id
}
func (c *serverSession) GetName() string {
	return c.name
}
func (c *serverSession) GetRPC() mqrpc.RPCClient {
	return c.rpc
}

// Deprecated: 因为命名规范问题函数将废弃,请用GetRPC代替
func (c *serverSession) GetRpc() mqrpc.RPCClient {
	return c.rpc
}

func (c *serverSession) GetApp() module.App {
	return c.app
}
func (c *serverSession) GetNode() *registry.Node {
	return c.node
}

func (c *serverSession) SetNode(node *registry.Node) (err error) {
	c.node = node
	return
}

/**
消息请求 需要回复
*/
func (c *serverSession) Call(ctx context.Context, _func string, params ...interface{}) (interface{}, string) {
	return c.rpc.Call(ctx, _func, params...)
}

/**
消息请求 不需要回复
*/
func (c *serverSession) CallNR(_func string, params ...interface{}) (err error) {
	return c.rpc.CallNR(_func, params...)
}

/**
消息请求 需要回复
*/
func (c *serverSession) CallArgs(ctx context.Context, _func string, ArgsType []string, args [][]byte) (interface{}, string) {
	return c.rpc.CallArgs(ctx, _func, ArgsType, args)
}

/**
消息请求 不需要回复
*/
func (c *serverSession) CallNRArgs(_func string, ArgsType []string, args [][]byte) (err error) {
	return c.rpc.CallNRArgs(_func, ArgsType, args)
}
