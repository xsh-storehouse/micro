package custom

import (
	"bufio"
	"errors"
	"fmt"
	"gitee.com/xsh-storehouse/micro/core/conf"
	"gitee.com/xsh-storehouse/micro/core/network"
	"math"
	"sync"
)

var notAlive = errors.New("Connection was dead")

type PackRecover interface {
	OnRecover(*Pack)
}
type Client struct {
	queue   *PackQueue
	recover PackRecover //消息接收者,从上层接口传过来的 只接收正式消息(心跳包,回复包等都不要)
	isStop  bool
	lock    *sync.Mutex
	curr_id int
}

func NewClient(conf conf.Custom, recover PackRecover, r *bufio.Reader, w *bufio.Writer, conn network.Conn, alive, MaxPackSize int) *Client {
	client := &Client{
		recover: recover,
		lock:    new(sync.Mutex),
		curr_id: 0,
	}
	client.queue = NewPackQueue(conf, r, w, conn, client.recvPack, alive, MaxPackSize)
	return client
}
func (c *Client) recvPack(pAndErr *packAndErr) (err error) {
	if pAndErr.err != nil {
		return pAndErr.err
	}
	//c.queue.WritePack(pAndErr.pack)
	c.recover.OnRecover(pAndErr.pack)
	return
}

// Push the msg and response the heart beat
func (c *Client) ListenLoop() (e error) {
	defer func() {
		if r := recover(); r != nil {

		}
	}()

	// Start the write queue
	go c.queue.Flusher()

	c.queue.ReadPackInLoop()

	c.lock.Lock()
	c.isStop = true
	c.lock.Unlock()
	return
}

func (c *Client) GetError() error {
	if c.queue == nil {
		return nil
	}
	return c.queue.writeError
}

func (c *Client) getOnlineMsgId() int {
	if c.curr_id == math.MaxUint16 {
		c.curr_id = 1
		return c.curr_id
	} else {
		c.curr_id = c.curr_id + 1
		return c.curr_id
	}
}

func (c *Client) WriteMsg(body []byte) error {
	if c.isStop {
		return fmt.Errorf("connection is closed")
	}
	return c.queue.WritePack(body)
}
