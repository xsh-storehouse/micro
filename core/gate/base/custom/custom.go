package custom

import (
	"bufio"
	"gitee.com/xsh-storehouse/micro/core/network"
	"io"
	"net"
	"time"
)

const (
	Rserved = iota
	PUBLISH //1
)
const (
	gDEFAULT_CONN_TIMEOUT     = 30 * time.Second       // Default connection timeout.
	gDEFAULT_RETRY_INTERVAL   = 100 * time.Millisecond // Default retry interval.
	gDEFAULT_READ_BUFFER_SIZE = 128                    // (Byte) Buffer size for reading.
	gDEFAULT_RECV_TIMEOUT     = time.Millisecond       // Default recv timeout.
)

type Pack struct {
	msg_type byte
	publish  Publish
	length   int
}

func (pack *Pack) GetPublish() interface{} {
	return pack.publish
}
func (pack *Pack) GetType() byte {
	return pack.msg_type
}
func (pack *Pack) SetType(typ byte) {
	pack.msg_type = typ
}

type Publish struct {
	topic_name string
	msg        []byte
}

func (pub *Publish) GetTopic() string {
	return pub.topic_name
}
func (pub *Publish) SetTopic(topic string) {
	pub.topic_name = topic
}
func (pub *Publish) GetMsg() []byte {
	return pub.msg
}
func (pub *Publish) SetMsg(msg []byte) {
	pub.msg = msg
}
func GetKeepAlive() int {
	return 30
}

func ReadPack(conn network.Conn, r *bufio.Reader, length int) (*Pack, error) {
	pack := new(Pack)
	var err error       // Reading error.
	var size int        // Reading size.
	var index int       // Received size.
	var buffer []byte   // Buffer object.
	var bufferWait bool // Whether buffer reading timeout set.

	if length > 0 {
		buffer = make([]byte, length)
	} else {
		buffer = make([]byte, gDEFAULT_READ_BUFFER_SIZE)
	}

	for {
		if length < 0 && index > 0 {
			bufferWait = true
			if err = conn.SetReadDeadline(time.Now().Add(gDEFAULT_RECV_TIMEOUT)); err != nil {
				return nil, err
			}
		}
		size, err = r.Read(buffer[index:])
		if size > 0 {
			index += size
			if length > 0 {
				// It reads til <length> size if <length> is specified.
				if index == length {
					break
				}
			} else {
				if index >= gDEFAULT_READ_BUFFER_SIZE {
					// If it exceeds the buffer size, it then automatically increases its buffer size.
					buffer = append(buffer, make([]byte, gDEFAULT_READ_BUFFER_SIZE)...)
				} else {
					// It returns immediately if received size is lesser than buffer size.
					if !bufferWait {
						break
					}
				}
			}
		}
		if err != nil {
			// Connection closed.
			if err == io.EOF {
				break
			}
			err = nil
			break
		}
		// Just read once from buffer.
		if length == 0 {
			break
		}
	}
	pack.length = index
	pack.publish.SetMsg(buffer[:index])
	return pack, err
}

// isTimeout checks whether given <err> is a timeout error.
func isTimeout(err error) bool {
	if err == nil {
		return false
	}
	if netErr, ok := err.(net.Error); ok && netErr.Timeout() {
		return true
	}
	return false
}
