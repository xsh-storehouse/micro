package custom

import (
	"bufio"
	"fmt"
	"gitee.com/xsh-storehouse/micro/core/conf"
	"gitee.com/xsh-storehouse/micro/core/network"
	"sync"
	"time"
)

// Tcp write queue
type PackQueue struct {
	conf conf.Custom
	// The last error in the tcp connection
	writeError error
	// Notice read the error
	fch       chan struct{}
	writelock sync.Mutex
	recover   func(pAndErr *packAndErr) (err error)
	// Pack connection
	r *bufio.Reader
	w *bufio.Writer

	conn network.Conn

	alive int

	MaxPackSize int //包最大长度

	status int
}
type packAndErr struct {
	pack *Pack
	err  error
}

// 1 is delay, 0 is no delay, 2 is just flush.
const (
	NO_DELAY = iota
	DELAY
	FLUSH

	DISCONNECTED = iota
	CONNECTED
	CLOSED
	RECONNECTING
	CONNECTING
)

// Init a pack queue
func NewPackQueue(conf conf.Custom, r *bufio.Reader, w *bufio.Writer, conn network.Conn, recover func(pAndErr *packAndErr) (err error), alive, MaxPackSize int) *PackQueue {
	if alive < 1 {
		alive = conf.ReadTimeout
	}
	if MaxPackSize < 1 {
		MaxPackSize = 65535
	}
	alive = int(float32(alive)*1.5 + 1)
	return &PackQueue{
		conf:        conf,
		alive:       alive,
		MaxPackSize: MaxPackSize,
		r:           r,
		w:           w,
		conn:        conn,
		recover:     recover,
		fch:         make(chan struct{}, 256),
		status:      CONNECTED,
	}
}

func (queue *PackQueue) isConnected() bool {
	return queue.status == CONNECTED
}

// Get a read pack queue
// Only call once
func (queue *PackQueue) Flusher() {
	for queue.isConnected() {
		if _, ok := <-queue.fch; !ok {
			break
		}
		queue.writelock.Lock()
		if !queue.isConnected() {
			queue.writelock.Unlock()
			break
		}
		if queue.w.Buffered() > 0 {
			if err := queue.w.Flush(); err != nil {
				queue.writelock.Unlock()
				break
			}
		}
		queue.writelock.Unlock()
	}
	//log.Info("flusher_loop Groutine will esc.")
}

func (queue *PackQueue) WritePack(bs []byte) error {
	_, err := queue.conn.Write(bs)
	if err != nil {
		return err
	} else {
		return nil
	}
}

func (queue *PackQueue) SetAlive(alive int) error {
	if alive < 1 {
		alive = queue.conf.ReadTimeout
	}
	alive = int(float32(alive)*1.5 + 1)
	queue.alive = alive
	return nil
}

// Get a read pack queue
// Only call once
func (queue *PackQueue) ReadPackInLoop() {
	p := new(packAndErr)
loop:
	for queue.isConnected() {
		if queue.alive > 0 {
			timeout := int(float64(queue.alive) * 3)
			if timeout > 60 {
				timeout = 60
			} else if timeout < 10 {
				timeout = 10
			}
			queue.conn.SetDeadline(time.Now().Add(time.Second * time.Duration(timeout)))
		} else {
			queue.conn.SetDeadline(time.Now().Add(time.Second * 90))
		}
		p.pack, p.err = ReadPack(queue.conn, queue.r, -1)
		if p.err != nil {
			queue.Close(p.err)
			break loop
		}
		err := queue.recover(p)
		if err != nil {
			queue.Close(err)
			break loop
		}
		p = new(packAndErr)
	}
	//log.Info("read_loop Groutine will esc.")
}
func (queue *PackQueue) CloseFch() {
	defer func() {
		if recover() != nil {
			// close(ch) panic occur
		}
	}()

	close(queue.fch) // panic if ch is closed
}

// Close the all of queue's channels
func (queue *PackQueue) Close(err error) error {
	queue.writeError = err
	queue.CloseFch()
	queue.status = CLOSED
	return nil
}

// Buffer
type buffer struct {
	index int
	data  []byte
}

func newBuffer(data []byte) *buffer {
	return &buffer{
		data:  data,
		index: 0,
	}
}
func (b *buffer) readString(length int) (s string, err error) {
	if (length + b.index) > len(b.data) {
		err = fmt.Errorf("Out of range error:%s", length)
		return
	}
	s = string(b.data[b.index:(length + b.index)])
	b.index += length
	return
}
func (b *buffer) readByte() (c byte, err error) {
	if (1 + b.index) > len(b.data) {
		err = fmt.Errorf("Out of range error")
		return
	}
	c = b.data[b.index]
	b.index++
	return
}
