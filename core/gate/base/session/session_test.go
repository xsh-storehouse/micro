package session

import (
	"fmt"
	"github.com/golang/protobuf/proto"
	"sync"
	"testing"
)

func TestSession(t *testing.T) {
	session := &SessionImp{ // 使用辅助函数设置域的值
		IP:      *proto.String("127.0.0.1"),
		Network: *proto.String("tcp"),
	} // 进行编码
	session.Settings = map[string]string{"isLogin": "true"}
	data, err := proto.Marshal(session)
	if err != nil {
		t.Fatalf("marshaling error: %s", err)
	} // 进行解码
	newSession := &SessionImp{}
	err = proto.Unmarshal(data, newSession)
	if err != nil {
		t.Fatalf("unmarshaling error: %s", err)
	} // 测试结果
	if newSession.GetSettings() == nil {
		t.Fatalf("data mismatch Settings == nil")
	} else {
		if newSession.GetSettings()["isLogin"] != "true" {
			t.Fatalf("data mismatch %q != %q", session.GetSettings()["isLogin"], newSession.GetSettings()["isLogin"])
		}
	}

}

func TestSessionagent_Serializable(t *testing.T) {
	session, err := NewSessionByMap(nil, map[string]interface{}{
		"IP": "IP",
	})
	if err != nil {
		t.Fatalf("NewSessionByMap error: %s", err)
	}
	var wg sync.WaitGroup
	wg.Add(1)
	go func() { //開一個協程寫map
		for j := 0; j < 1000000; j++ {
			_session := session.Clone()
			session.Serializable()
			session.SetLocalKV("ff", "sss")
			_session.Set("TestTopic", fmt.Sprintf("set %s", j))
			_session.SetTopic("ttt")
			_session.Serializable()
		}
		wg.Done()
	}()
	wg.Add(1)
	go func() { //開一個協程讀map
		for j := 0; j < 1000000; j++ {
			session.Clone()
			session.Serializable()
			session.SetLocalKV("ff", "sss")
			session.SetTopic("ttt")
			//fmt.Println("Serializable", b)
		}
		wg.Done()
	}()
	wg.Wait()
}
