package gate

import (
	"bytes"
	"gitee.com/xsh-storehouse/micro/core/server"
	"time"
)

//Option 网关配置项
type Option func(*Options)

/**
CustomUnpack 自定义的解包方法
参数:需要处理的字节
返回:
	1.处理完毕的包
	2.剩余的字节
*/
type CustomUnpack func([]byte) ([]*bytes.Buffer, int)

//Options 网关配置项
type Options struct {
	ConcurrentTasks int
	BufSize         int
	MaxPackSize     int
	TLS             bool
	TCPAddr         string
	WsAddr          string
	CertFile        string
	KeyFile         string
	Heartbeat       time.Duration
	OverTime        time.Duration
	RouteHandler    RouteHandler
	StorageHandler  StorageHandler
	AgentLearner    AgentLearner
	SessionLearner  SessionLearner
	GateHandler     GateHandler
	SendMessageHook SendMessageHook
	Opts            []server.Option
	Unpack          CustomUnpack
	PackOrder       bool
}

//NewOptions 网关配置项
func NewOptions(opts ...Option) Options {
	opt := Options{
		Opts:            []server.Option{},
		ConcurrentTasks: 20,
		BufSize:         2048,
		MaxPackSize:     65535,
		Heartbeat:       time.Minute,
		OverTime:        time.Second * 10,
		TLS:             false,
	}

	for _, o := range opts {
		o(&opt)
	}

	return opt
}

//ConcurrentTasks 设置单个连接允许的同时并发协程数
func ConcurrentTasks(s int) Option {
	return func(o *Options) {
		o.ConcurrentTasks = s
	}
}

//BufSize 单个连接网络数据缓存大小
func BufSize(s int) Option {
	return func(o *Options) {
		o.BufSize = s
	}
}

//MaxPackSize 单个协议包数据最大值
func MaxPackSize(s int) Option {
	return func(o *Options) {
		o.MaxPackSize = s
	}
}

//Heartbeat 心跳时间
func Heartbeat(s time.Duration) Option {
	return func(o *Options) {
		o.Heartbeat = s
	}
}

//OverTime 超时时间
func OverTime(s time.Duration) Option {
	return func(o *Options) {
		o.OverTime = s
	}
}

//SetRouteHandler 设置路由器
func SetRouteHandler(s RouteHandler) Option {
	return func(o *Options) {
		o.RouteHandler = s
	}
}

//SetStorageHandler 设置session管理器
func SetStorageHandler(s StorageHandler) Option {
	return func(o *Options) {
		o.StorageHandler = s
	}
}

//SetAgentLearner SetAgentLearner(不要使用,建议用SetSessionLearner)
func SetAgentLearner(s AgentLearner) Option {
	return func(o *Options) {
		o.AgentLearner = s
	}
}

//SetGateHandler SetGateHandler
func SetGateHandler(s GateHandler) Option {
	return func(o *Options) {
		o.GateHandler = s
	}
}

//SetSessionLearner SetSessionLearner
func SetSessionLearner(s SessionLearner) Option {
	return func(o *Options) {
		o.SessionLearner = s
	}
}

//SetSendMessageHook SetSendMessageHook
func SetSendMessageHook(s SendMessageHook) Option {
	return func(o *Options) {
		o.SendMessageHook = s
	}
}

//Tls Tls
// Deprecated: 因为命名规范问题函数将废弃,请用TLS代替
func Tls(s bool) Option {
	return func(o *Options) {
		o.TLS = s
	}
}

//TLS TLS
func TLS(s bool) Option {
	return func(o *Options) {
		o.TLS = s
	}
}

// TcpAddr tcp监听地址
// Deprecated: 因为命名规范问题函数将废弃,请用TCPAddr代替
func TcpAddr(s string) Option {
	return func(o *Options) {
		o.TCPAddr = s
	}
}

// TCPAddr tcp监听端口
func TCPAddr(s string) Option {
	return func(o *Options) {
		o.TCPAddr = s
	}
}

// WsAddr websocket监听端口
func WsAddr(s string) Option {
	return func(o *Options) {
		o.WsAddr = s
	}
}

// CertFile TLS 证书cert文件
func CertFile(s string) Option {
	return func(o *Options) {
		o.CertFile = s
	}
}

// KeyFile TLS 证书key文件
func KeyFile(s string) Option {
	return func(o *Options) {
		o.KeyFile = s
	}
}

// ServerOpts ServerOpts
func ServerOpts(s []server.Option) Option {
	return func(o *Options) {
		o.Opts = s
	}
}

//解包
func UnpackOpts(fuc CustomUnpack) Option {
	return func(o *Options) {
		o.Unpack = fuc
	}
}

//包传递顺序
func PackOrder(s bool) Option {
	return func(o *Options) {
		o.PackOrder = s
	}
}
