package rpcpb

import (
	"github.com/golang/protobuf/proto"
	"testing"
	"time"
)

func TestRPCInfo(t *testing.T) {
	rpc := &RPCInfo{ // 使用辅助函数设置域的值
		Cid:     *proto.String("123457"),
		Fn:      *proto.String("hello"),
		Expired: *proto.Int64(time.Now().UnixNano() / 1000000),
		Reply:   *proto.Bool(true),
		ReplyTo: *proto.String("232244"),
	} // 进行编码
	rpc.ArgsType = []string{"s", "s"}
	rpc.Args = [][]byte{[]byte("hello"), []byte("world")}
	data, err := proto.Marshal(rpc)
	if err != nil {
		t.Fatalf("marshaling error: ", err)
	} // 进行解码
	newRPC := &RPCInfo{}
	err = proto.Unmarshal(data, newRPC)
	if err != nil {
		t.Fatalf("unmarshaling error: ", err)
	} // 测试结果
	if rpc.ReplyTo != newRPC.GetReplyTo() {
		t.Fatalf("data mismatch %q != %q", rpc.GetReplyTo(), newRPC.GetReplyTo())
	}
}

func TestResultInfo(t *testing.T) {
	result := &ResultInfo{ // 使用辅助函数设置域的值
		Cid:        *proto.String("123457"),
		Error:      *proto.String("hello"),
		ResultType: *proto.String("s"),
		Result:     []byte("232244"),
	} // 进行编码
	data, err := proto.Marshal(result)
	if err != nil {
		t.Fatalf("marshaling error: ", err)
	} // 进行解码
	newResult := &ResultInfo{}
	err = proto.Unmarshal(data, newResult)
	if err != nil {
		t.Fatalf("unmarshaling error: ", err)
	} // 测试结果
	if result.Cid != newResult.GetCid() {
		t.Fatalf("data mismatch %q != %q", result.GetCid(), newResult.GetCid())
	}
}
