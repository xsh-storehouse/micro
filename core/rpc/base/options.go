package defaultrpc

import "gitee.com/xsh-storehouse/micro/core/rpc/pb"

type ClinetCallInfo struct {
	correlation_id string
	timeout        int64 //超时
	call           chan *rpcpb.ResultInfo
}
