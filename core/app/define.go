package app

import (
	"gitee.com/xsh-storehouse/micro/core/conf"
	"gitee.com/xsh-storehouse/micro/core/module"
	"sync"
)

type resultInfo struct {
	Trace   string      `json:"trace"`
	Error   string      `json:"error"`  //错误结果 如果为nil表示请求正确
	Result  interface{} `json:"result"` //结果
	Code    int         `json:"code"`   //0:错误 1:正确
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type protocolMarshalImp struct {
	data []byte
}

// DefaultApp 默认应用
type DefaultApp struct {
	//module.App
	version       string
	settings      conf.Config
	serverList    sync.Map
	opts          module.Options
	defaultRoutes func(app module.App, Type string, hash string) module.ServerSession
	//将一个RPC调用路由到新的路由上
	mapRoute            func(app module.App, route string) string
	rpcserializes       map[string]module.RPCSerialize
	configurationLoaded func(app module.App)
	startup             func(app module.App)
	moduleInited        func(app module.App, module module.Module)
	protocolMarshal     func(Trace string, Result interface{}, Error string) (module.ProtocolMarshal, string)
}
