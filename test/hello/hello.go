/**
一定要记得在confin.json配置这个模块的参数,否则无法使用
*/
package hello

import (
	"fmt"
	"gitee.com/xsh-storehouse/micro/core/conf"
	"gitee.com/xsh-storehouse/micro/core/gate"
	"gitee.com/xsh-storehouse/micro/core/module"
	basemodule "gitee.com/xsh-storehouse/micro/core/module/base"
	"gitee.com/xsh-storehouse/micro/utils/log"
)

var Init = func() module.Module {
	this := new(HellWorld)
	return this
}

type HellWorld struct {
	basemodule.Module
}

func (w *HellWorld) GetType() string {
	//很关键,需要与配置文件中的Module配置对应
	return "hello"
}
func (w *HellWorld) Version() string {
	//可以在监控时了解代码版本
	return "1.0.0"
}
func (w *HellWorld) OnAppConfigurationLoaded(app module.App) {
	//当App初始化时调用，这个接口不管这个模块是否在这个进程运行都会调用
	w.Module.OnAppConfigurationLoaded(app)
}
func (w *HellWorld) OnInit(app module.App, settings *conf.Mods) {
	w.Module.OnInit(w, app, settings)
	w.GetServer().Options().Metadata["state"] = "alive"
	w.GetServer().RegisterGO("_say", w._say)
	w.GetServer().RegisterGO("say", w.say)
	log.Info("%v模块初始化完成...", w.GetType())
}

func (w *HellWorld) Run(closeSig chan bool) {
	log.Info("%v模块运行中...", w.GetType())
	<-closeSig
	log.Info("%v模块已停止...", w.GetType())
}

func (w *HellWorld) OnDestroy() {
	//一定继承
	w.Module.OnDestroy()
	log.Info("%v模块已回收...", w.GetType())
}

func (w *HellWorld) say(session gate.Session, msg map[string]interface{}) (r string, err error) {
	return fmt.Sprintf("hello %v 你在网关 %v", msg["name"], session.GetServerID()), nil
}

func (w *HellWorld) _say(name interface{}) (r string, err error) {
	return fmt.Sprintf("hi %s 你在网关 %s", name, w.GetServerID()), nil
}
