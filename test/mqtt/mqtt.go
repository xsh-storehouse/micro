package mqtt

import (
	"context"
	"fmt"
	"gitee.com/xsh-storehouse/micro/core/conf"
	"gitee.com/xsh-storehouse/micro/core/gate"
	mqttgate "gitee.com/xsh-storehouse/micro/core/gate/base/mqtt"
	"gitee.com/xsh-storehouse/micro/core/module"
	mqrpc "gitee.com/xsh-storehouse/micro/core/rpc"
	"gitee.com/xsh-storehouse/micro/utils/log"
	"time"
)

var Init = func() module.Module {
	gate := new(Handle)
	return gate
}

type Handle struct {
	mqttgate.Gate //继承
}

func (g *Handle) GetType() string {
	//很关键,需要与配置文件中的Module配置对应
	return "mqtt"
}

func (g *Handle) Version() string {
	//可以在监控时了解代码版本
	return "1.0.0"
}

func (g *Handle) OnInit(app module.App, settings *conf.Mods) {
	g.Gate.OnInit(g, app, settings,
		gate.WsAddr(":5600"),
		gate.TCPAddr(":5500"),
	)
	g.Gate.GetServer().RegisterGO("do", g.do)
	log.Info("%s 模块初始化完成...", g.GetType())
}

func (g *Handle) do(session gate.Session, msg map[string]interface{}) (r string, err error) {
	//session.Send("/gate/send/test", []byte(fmt.Sprintf("这是回传信息 %s", msg["name"])))
	ctx, _ := context.WithTimeout(context.TODO(), time.Second*3) //3s后超时
	rs, err := mqrpc.String(
		g.Call(
			ctx,
			"hello", //要访问的moduleType
			"_say",  //访问模块中handler路径
			mqrpc.Param(msg["name"]),
		),
	)
	if err != nil {
		log.Error("%s", err.Error())
	}
	return fmt.Sprintf("mqtt:你在网关%s,msg:%s", session.GetServerID(), rs), nil
}
