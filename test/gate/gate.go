//基于uri协议的路由规则
package gate

import (
	"fmt"
	"gitee.com/xsh-storehouse/micro/core/conf"
	"gitee.com/xsh-storehouse/micro/core/gate"
	mqttgate "gitee.com/xsh-storehouse/micro/core/gate/base/mqtt"
	"gitee.com/xsh-storehouse/micro/core/gate/uriroute"
	"gitee.com/xsh-storehouse/micro/core/module"
	"gitee.com/xsh-storehouse/micro/utils/log"
	"time"
)

var Init = func() module.Module {
	gate := new(Handle)
	return gate
}

type Handle struct {
	mqttgate.Gate //继承
	Route         *uriroute.URIRoute
}

func (g *Handle) GetType() string {
	//很关键,需要与配置文件中的Module配置对应
	return "gateway"
}

func (g *Handle) Version() string {
	//可以在监控时了解代码版本
	return "1.0.0"
}

func (g *Handle) OnInit(app module.App, settings *conf.Mods) {
	g.Route = uriroute.NewURIRoute(g,
		uriroute.CallTimeOut(3*time.Second),
	)
	g.Gate.OnInit(g, app, settings,
		gate.SetRouteHandler(g.Route),
		gate.WsAddr(":6600"),
		gate.TCPAddr(":6500"),
	)
	g.Gate.GetServer().RegisterGO("/do", g.do)
	log.Info("%s 模块初始化完成...", g.GetType())
}

func (g *Handle) do(session gate.Session, msg map[string]interface{}) (r string, err error) {
	session.Send("/gate/send/test", []byte(fmt.Sprintf("这是回传信息 %s", msg["name"])))
	return fmt.Sprintf("gate:你在网关%s", session.GetServerID()), nil
}
