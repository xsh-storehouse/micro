package robot

import (
	"gitee.com/xsh-storehouse/micro/utils/task"
	"github.com/gogf/gf/os/glog"
	"github.com/gogf/gf/os/gtime"
)

type Manager struct {
	Start *gtime.Time
}
type Work struct {
	manager *Manager
	Start   *gtime.Time
}

func (m *Manager) CreateWork() task.Work {
	m.Start = gtime.Now()
	return NewWork(m)
}

func (m *Manager) Finish() {
	total := gtime.Now().Sub(m.Start)
	glog.Info(total)
}

func Run(t task.Loop) {
	this := new(Manager)
	glog.Info("开始压测请等待...")
	t.Run(this)
}
