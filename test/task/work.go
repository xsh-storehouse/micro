package robot

import (
	"gitee.com/xsh-storehouse/micro/utils/task"
	"github.com/gogf/gf/os/glog"
	"github.com/gogf/gf/os/gtime"
)

func NewWork(manager *Manager) *Work {
	this := new(Work)
	this.manager = manager
	this.Start = gtime.Now()
	return this
}

func (w Work) Init(task task.Task) {

}

func (w Work) RunWorker(task task.Task, index int) {
	glog.Infof("RunWorker %d", index)
}

func (w Work) Close(task task.Task) {

}
