package web

import (
	"gitee.com/xsh-storehouse/micro/core/conf"
	"gitee.com/xsh-storehouse/micro/core/module"
	webgate "gitee.com/xsh-storehouse/micro/core/web"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/util/gconv"
)

const _name = "gf"

var Init = func() module.Module {
	this := new(Web)
	return this
}

type Web struct {
	webgate.GF
}

func (w *Web) Version() string {
	return "1.0.0"
}

func (w *Web) GetType() string {
	return _name
}

func (w *Web) OnInit(app module.App, settings *conf.Mods) {
	w.GF.OnInit(w, app, settings,
		webgate.Name(w.GetType()),
		webgate.HttpPort(gconv.Int(g.Cfg().Get("server.HttpPort"))),
		webgate.HttpsPort(gconv.Int(g.Cfg().Get("server.HttpsPort"))),
	)
}
