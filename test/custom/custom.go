package custom

import (
	"bytes"
	"encoding/binary"
	"gitee.com/xsh-storehouse/micro/core/conf"
	"gitee.com/xsh-storehouse/micro/core/gate"
	customgate "gitee.com/xsh-storehouse/micro/core/gate/base/custom"
	"gitee.com/xsh-storehouse/micro/core/module"
	Byte "gitee.com/xsh-storehouse/micro/utils/byte"
	"gitee.com/xsh-storehouse/micro/utils/log"
	"github.com/gogf/gf/os/glog"
)

var Init = func() module.Module {
	gate := new(Handle)
	return gate
}

type Handle struct {
	customgate.Gate //继承
}

func (g *Handle) GetType() string {
	//很关键,需要与配置文件中的Module配置对应
	return "custom"
}

func (g *Handle) Version() string {
	//可以在监控时了解代码版本
	return "1.0.0"
}

func (g *Handle) OnInit(app module.App, settings *conf.Mods) {
	g.Gate.OnInit(g, app, settings,
		gate.WsAddr(":4600"),
		gate.TCPAddr(":4500"),
		gate.UnpackOpts(unpack1),
		gate.PackOrder(true),
	)
	g.Gate.GetServer().RegisterGO("dispose", g.dispose1)
	log.Info("%s 模块初始化完成...", g.GetType())
}

func (g *Handle) dispose1(session gate.Session, msg []byte) (r string, err error) {
	glog.Infof("自定义数据需要解析 %s", Byte.ToStringHexSpace(msg, len(msg)))
	return
}

func (g *Handle) dispose2(session gate.Session, msg map[string]interface{}) (r string, err error) {
	glog.Infof("自定义数据需要解析 %s", msg["name"])
	return
}

//解包:帧头+长度
func unpack1(bs []byte) ([]*bytes.Buffer, int) {
	const _MIN = 4
	var pgs []*bytes.Buffer
	var index int
	var ret int
	size := len(bs)
	for index <= size-_MIN {
		b1 := bs[index]
		index++
		if b1 != 0xd5 {
			continue
		} else {
			b2 := bs[index]
			index++
			if b2 != 0xc8 {
				continue
			} else {
				length := int(binary.BigEndian.Uint16(bs[index : index+2]))
				index += 2
				if len(bs[index:]) >= length {
					pg := bytes.NewBuffer(bs[index : index+length])
					pgs = append(pgs, pg)
					index += length
				} else {
					index -= _MIN
					break
				}
			}
		}
	}
	ret = size - index
	return pgs, ret
}

//解包:json 结尾带回车换行 \r\n
func unpack2(bs []byte) ([]*bytes.Buffer, int) {
	var pgs []*bytes.Buffer
	var ret int
	var start int
	var index int
	size := len(bs)
	for i, v := range bs {
		index++
		if v == 10 && bs[i-1] == 13 && bs[i-2] == 125 { //遇到}\r\n
			pg := bytes.NewBuffer(bs[start : index+start-2])
			start = index
			pgs = append(pgs, pg)
		}
	}
	ret = size - start
	return pgs, ret
}
