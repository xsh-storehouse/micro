package micro

import (
	"gitee.com/xsh-storehouse/micro/core/module"
	"gitee.com/xsh-storehouse/micro/core/registry"
	"gitee.com/xsh-storehouse/micro/core/registry/consul"
	robot "gitee.com/xsh-storehouse/micro/test/task"
	"gitee.com/xsh-storehouse/micro/test/web"
	"gitee.com/xsh-storehouse/micro/utils/log"
	"gitee.com/xsh-storehouse/micro/utils/task"
	"github.com/gogf/gf/os/gtime"
	. "github.com/gogf/gf/test/gtest"
	"github.com/gogf/gf/text/gstr"
	"github.com/nats-io/nats.go"
	"net/http"
	_ "net/http/pprof"
	"testing"
	"time"
)

func TestMicro(t *testing.T) {
	go func() {
		err := http.ListenAndServe(":6060", nil)
		if err != nil {
			log.Error("", err.Error())
		}
	}()

	rs := consul.NewRegistry(func(options *registry.Options) {
		options.Addrs = []string{"127.0.0.1:8500"}
	})
	//rs:=etcdv3.NewRegistry(func(options *registry.Options) {
	//	options.Addrs = []string{"127.0.0.1:2379"}
	//})
	nc, err := nats.Connect("nats://127.0.0.1:4222", nats.MaxReconnects(10000))
	if err != nil {
		log.Error("nats error %s", err.Error())
		return
	}
	app := CreateApp(
		module.KillWaitTTL(1*time.Minute),
		module.Debug(false),
		module.Nats(nc),     //指定nats rpc
		module.Registry(rs), //指定服务发现
		module.RegisterTTL(20*time.Second),
		module.RegisterInterval(10*time.Second),
	)
	//err = app.Run(hello.Init(), mqtt.Init(), custom.Init())
	err = app.Run(web.Init())
	if err != nil {
		log.Error("%s", err.Error())
	}
}

func TestTask(t *testing.T) {
	C(t, func(t *T) {
		Assert(gstr.Trim(" 123456\n "), "123456")
		Assert(gstr.Trim("#123456#;", "#;"), "123456")
	})
	task := task.Loop{
		Start: gtime.Now(),
		Count: 1000, //并发数
	}
	robot.Run(task)
}
