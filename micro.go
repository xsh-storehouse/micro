package micro

import (
	"gitee.com/xsh-storehouse/micro/core/app"
	"gitee.com/xsh-storehouse/micro/core/module"
	"gitee.com/xsh-storehouse/micro/utils/log"
)

const version = "1.0.0"

//创建实例
func CreateApp(opts ...module.Option) module.App {
	log.Init(true)
	opts = append(opts, module.Version(version))
	return app.NewApp(opts...)
}
