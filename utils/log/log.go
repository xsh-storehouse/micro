package log

import (
	"fmt"
	"github.com/gogf/gf/os/glog"
)

func Init(debug bool) {
	if debug == true {
		glog.SetLevel(glog.LEVEL_DEV)
	} else {
		glog.SetLevel(glog.LEVEL_INFO | glog.LEVEL_WARN | glog.LEVEL_ERRO)
	}
	glog.SetAsync(true)
	glog.SetFlags(glog.F_TIME_STD | glog.F_FILE_SHORT)
}

// CreateTrace CreateTrace
func CreateTrace(trace, span string) TraceSpan {
	return &TraceSpanImp{
		Trace: trace,
		Span:  span,
	}
}

func Debug(format string, a ...interface{}) {
	if format == "" {
		glog.Skip(1).Debug(a...)
	} else {
		glog.Skip(1).Debugf(format, a...)
	}
}

func Info(format string, a ...interface{}) {
	if format == "" {
		glog.Skip(1).Info(a...)
	} else {
		glog.Skip(1).Infof(format, a...)
	}
}

func Error(format string, a ...interface{}) {
	if format == "" {
		glog.Skip(1).Error(a)
	} else {
		glog.Skip(1).Errorf(format, a)
	}
}

func Warning(format string, a ...interface{}) {
	if format == "" {
		glog.Skip(1).Warning(a...)
	} else {
		glog.Skip(1).Warningf(format, a...)
	}
}

// TInfo TInfo
func TInfo(span TraceSpan, format string, a ...interface{}) {
	if span != nil {
		Info(fmt.Sprintf("[%s][%s] %s", span.SpanID(), span.TraceID(), format), a)
	} else {
		Info(format, a)
	}
}
