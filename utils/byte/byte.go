package byte

import (
	"bytes"
	"strconv"
)

func ToStringHexSpace(buf []byte, reqLen int) string {
	buffer := new(bytes.Buffer)
	for _, b := range buf[:reqLen] {
		s := strconv.FormatInt(int64(b&0xff), 16)
		if len(s) == 1 {
			buffer.WriteString("0")
		}
		buffer.WriteString(s)
		buffer.WriteString(" ")
	}
	return buffer.String()
}

func ToStringHex(buf []byte, reqLen int) string {
	buffer := new(bytes.Buffer)
	for _, b := range buf[:reqLen] {
		s := strconv.FormatInt(int64(b&0xff), 16)
		if len(s) == 1 {
			buffer.WriteString("0")
		}
		buffer.WriteString(s)
	}
	return buffer.String()
}
