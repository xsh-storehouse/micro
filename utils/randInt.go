package microtools

import "math/rand"

// RandInt64 生成一个min->max的随机数
func RandInt64(min, max int64) int64 {
	if min >= max {
		return max
	}
	return rand.Int63n(max-min) + min
}
