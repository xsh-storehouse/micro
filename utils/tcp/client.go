package tcp

import (
	"github.com/gogf/gf/container/gqueue"
	"github.com/gogf/gf/net/gtcp"
	"github.com/gogf/gf/os/glog"
	"github.com/gogf/gf/os/gtime"
	"github.com/gogf/gf/os/gtimer"
	"github.com/gogf/gf/util/gconv"
	"time"
)

type Client struct {
	Name         string
	Address      string
	TimeOut      int
	Conn         *gtcp.Conn
	DownQueue    *gqueue.Queue
	MessageQueue *gqueue.Queue

	Running bool
}

func (c *Client) Run(name string, address string, timeOut int) {
	c.Name = name
	c.Address = address
	c.TimeOut = timeOut
	c.Running = true
	c.DownQueue = gqueue.New()
	c.MessageQueue = gqueue.New()
	go c.Start()
	go c.loop()
}

func (c *Client) Start() {
	time.Sleep(time.Second)
	for c.Running {
		if conn, err := gtcp.NewConn(c.Address); err == nil {
			c.MessageQueue.Push(Message{Code: ConnectSuccess})
			c.Conn = conn
			updateTime := gtime.Now()
			glog.Infof("[%s] %s 连接成功", c.Name, c.Address)
			timer := gtimer.Add(10*time.Second, func() {
				if c.TimeOut != 0 {
					if gtime.Now().Sub(updateTime).Seconds() > gconv.Float64(c.TimeOut) {
						conn.Close()
					}
				}
			})
			timer.Run()
			for {
				data, err := conn.RecvPkg()
				if err != nil {
					if err.Error() == "EOF" {
						glog.Infof("[%s] 连接已经关闭", c.Name)
					}
					break
				}
				updateTime = gtime.Now()
				c.MessageQueue.Push(Message{Code: ConnectMessage, Pload: data})
			}
			timer.Close()
			conn.Close()
			c.MessageQueue.Push(Message{Code: ConnectFailed})
			glog.Infof("[%s] 连接退出", c.Name)
		} else {
			glog.Info(err)
		}
		time.Sleep(5 * time.Second)
	}
}

func (c *Client) Stop() {
	c.Running = false
}

func (c *Client) Send(data Data) {
	c.DownQueue.Push(data)
}

func (c *Client) loop() {
	for c.Running {
		if v := c.DownQueue.Pop(); v != nil {
			t := v.(Data)
			if len(t.Pload) > 65535 {
				glog.Infof("[%s] 发送的包字节长度超过65535,请拆包后发送", c.Name)
			} else {
				if err := c.Conn.SendPkg(t.Pload); err != nil {
					glog.Errorf("[%s] %s", c.Name, err.Error())
				}
			}
		}
	}
}
