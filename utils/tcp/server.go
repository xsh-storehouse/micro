package tcp

import (
	"errors"
	"fmt"
	"github.com/gogf/gf/container/gmap"
	"github.com/gogf/gf/container/gqueue"
	"github.com/gogf/gf/net/gtcp"
	"github.com/gogf/gf/os/glog"
	"github.com/gogf/gf/os/gmutex"
	"github.com/gogf/gf/os/grpool"
	"github.com/gogf/gf/os/gtime"
	"github.com/gogf/gf/os/gtimer"
	"github.com/gogf/gf/util/gconv"
	"github.com/gogf/gf/util/gutil"
	"time"
)

type Service struct {
	Name         string
	Address      string
	TimeOut      int
	Running      bool
	Handle       *gtcp.Server
	ConnMap      *gmap.TreeMap
	DownQueue    *gqueue.Queue
	MessageQueue *gqueue.Queue
	mutex        *gmutex.Mutex
	pond         *grpool.Pool
}

func (c *Service) Run(name string, address string, timeOut int) {
	c.Name = name
	c.Address = address
	c.TimeOut = timeOut
	c.Running = true
	c.ConnMap = gmap.NewTreeMap(gutil.ComparatorString, true)
	c.DownQueue = gqueue.New()
	c.MessageQueue = gqueue.New()
	c.mutex = gmutex.New()
	c.pond = grpool.New(100)
	go c.Start()
	go c.loop()
}

func (c *Service) Start() {
	c.Handle = gtcp.NewServer(c.Address, func(conn *gtcp.Conn) {
		var key uint64
		var timer *gtimer.Entry
		remoteAddr := conn.RemoteAddr().String()
		defer func() {
			conn.Close()
			timer.Close()
			glog.Infof("%s 接入退出:%s", c.Name, remoteAddr)
			c.MessageQueue.Push(Message{Code: ConnectFailed, Pload: key})
			c.pond.Add(func() {
				time.Sleep(5 * time.Second)
				if c.ConnMap.Contains(key) {
					c.ConnMap.Remove(key)
				}
			})
		}()
		key = c.getHandle()
		if key != 0 {
			box := ConnectBox{
				Key:     key,
				Address: remoteAddr,
				Conn:    conn,
			}
			c.ConnMap.Set(key, box)
			glog.Infof("%s 新的连接:%s", c.Name, remoteAddr)
			c.MessageQueue.Push(Message{Code: ConnectSuccess, Pload: key})
			updateTime := gtime.Now()
			timer = gtimer.Add(10*time.Second, func() {
				if gtime.Now().Sub(updateTime).Seconds() > gconv.Float64(c.TimeOut) {
					conn.Close()
				}
			})
			for {
				data, err := conn.RecvPkg()
				if err != nil {
					if err.Error() == "EOF" {
						glog.Infof("%s %s 连接已经关闭", c.Name, remoteAddr)
					}
					break
				}
				updateTime = gtime.Now()
				c.MessageQueue.Push(Message{Code: ConnectMessage, Pload: Data{Pload: data, IntHandle: key}})
			}
		} else {
			glog.Error("没有获取到句柄")
		}
	})
	glog.Infof("%s %s 启动", c.Name, c.Address)
	err := c.Handle.Run()
	if err != nil {
		glog.Error(err)
	}
}
func (c *Service) Close(key uint64) {
	if c.ConnMap.Contains(key) {
		t := c.ConnMap.Get(key)
		if t != nil {
			d := t.(ConnectBox)
			d.Conn.Close()
		}
	}
}
func (c *Service) Stop() {
	c.Running = false
	c.Handle.Close()
	c.ConnMap.Clear()
	c.DownQueue.Close()
	glog.Infof("%s %s 退出", c.Name, c.Address)
}

func (c *Service) Send(data Data) error {
	if c.ConnMap.Contains(data.IntHandle) {
		c.DownQueue.Push(data)
		return nil
	} else {
		return errors.New(fmt.Sprintf("%s %d 没有找到连接", c.Name, data.IntHandle))
	}
}

func (c *Service) GetAddr(key uint64) string {
	if c.ConnMap.Contains(key) {
		tmp := c.ConnMap.Get(key)
		if tmp != nil {
			box := tmp.(ConnectBox)
			return box.Address
		} else {
			return ""
		}
	} else {
		return ""
	}
}

func (c *Service) SetData(key uint64, data interface{}) {
	if c.ConnMap.Contains(key) {
		tmp := c.ConnMap.Get(key)
		if tmp != nil {
			box := tmp.(ConnectBox)
			box.Data = data
			c.ConnMap.Set(key, box)
		}
	}
}

func (c *Service) GetData(key uint64) interface{} {
	if c.ConnMap.Contains(key) {
		tmp := c.ConnMap.Get(key)
		if tmp != nil {
			box := tmp.(ConnectBox)
			return box.Data
		} else {
			return nil
		}
	} else {
		return nil
	}
}
func (c *Service) GetConn(key uint64) (*gtcp.Conn, bool) {
	d := c.GetData(key)
	if d == nil {
		return nil, false
	} else {
		return d.(ConnectBox).Conn, true
	}
}
func (c *Service) loop() {
	for c.Running {
		if v := c.DownQueue.Pop(); v != nil {
			t := v.(Data)
			if c.ConnMap.Contains(t.IntHandle) {
				tmp := c.ConnMap.Get(t.IntHandle)
				if tmp != nil {
					box := tmp.(ConnectBox)
					if len(t.Pload) > 65535 {
						glog.Infof("%s 发送的包字节长度超过65535,请拆包后发送", c.Name)
					} else {
						if err := box.Conn.SendPkg(t.Pload); err != nil {
							glog.Errorf("%s %s", c.Name, err.Error())
						} else {

						}
					}
				}
			}
		}
	}
}

func (c *Service) getHandle() uint64 {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	i := 0
	for {
		handle := time.Now().UnixNano()
		if !c.ConnMap.Contains(handle) {
			return uint64(handle)
		}
		i++
		if i > 10 {
			time.Sleep(100 * time.Millisecond)
			continue
		}
		time.Sleep(10 * time.Millisecond)
	}
}
