package tcp

import "github.com/gogf/gf/net/gtcp"

const (
	ConnectSuccess = iota + 1
	ConnectFailed
	ConnectMessage
)

type Message struct {
	Code  int
	Pload interface{}
}

type ConnectBox struct {
	Key     uint64
	Address string
	Conn    *gtcp.Conn
	Data    interface{}
}

type Data struct {
	IntHandle uint64
	Pload     []byte
}
