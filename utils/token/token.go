package token

import (
	"fmt"
	"github.com/gogf/gf/os/gcache"
	"github.com/gogf/gf/util/gconv"
	"time"
)

var (
	Prefix    = "token"
	TokenTime = 3600 * 24
)

func SetToken(key string, token string, overtime int) {
	mkey := fmt.Sprintf("%s-%s", Prefix, key)
	gcache.Set(mkey, token, time.Duration(overtime))
}

func GetToken(key string) (token string) {
	mkey := fmt.Sprintf("%s-%s", Prefix, key)
	t, _ := gcache.Get(mkey)
	token = gconv.String(t)
	return
}

func RemoveToken(key string) {
	mkey := fmt.Sprintf("%s-%s", Prefix, key)
	has, err := gcache.Contains(mkey)
	if has && err == nil {
		gcache.Remove(mkey)
	}
}
