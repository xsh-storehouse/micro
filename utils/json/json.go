package json

import (
	"encoding/json"
	"fmt"
)

func Getter(call func() (string, error), v interface{}) error {
	str, err := call()
	if err != nil {
		return err
	}
	fmt.Println("Etcd-->", str)
	return json.Unmarshal([]byte(str), v)
}
