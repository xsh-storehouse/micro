package task

import (
	"github.com/gogf/gf/os/gtime"
	"sync"
)

type Loop struct {
	Count    int //并发数
	Start    *gtime.Time
	wg       sync.WaitGroup
	UserData interface{}
}

type Work interface {
	Init(task Task)
	RunWorker(task Task, index int)
}

type WorkManager interface {
	CreateWork() Work
	Finish()
}
