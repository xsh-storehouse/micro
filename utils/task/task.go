package task

import (
	"github.com/gogf/gf/os/gtime"
)

type Task interface {
}

func (b *Loop) Run(manager WorkManager) {
	b.Start = gtime.Now()
	b.runWorkers(manager)
}

func (b *Loop) runWorkers(manager WorkManager) {
	b.wg.Add(b.Count)
	for i := 0; i < b.Count; i++ {
		task := manager.CreateWork()
		go func(index int) {
			task.Init(b)
			task.RunWorker(b, index)
			b.wg.Done()
		}(i)
	}
	b.wg.Wait()
	manager.Finish()
}
