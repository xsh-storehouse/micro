# micro

#### 介绍
gf、go-micro基于这2个框架开发的基础框架，主体以go-micro，使用gf工具

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

#### windows启动服务
sc create consul binpath= "C:\soft\consul_1.9.3_windows_amd64\consul.exe agent -dev"
sc create nats binpath= "C:\soft\nats-server-v2.1.9-windows-amd64\nats-server.exe agent -dev"

#### 问题
1. 解决低版本 GoLand 调试报 Version of Delve is too old for this version of Go
https://jpanj.com/2020/resolve-old-goland-delve-bug/